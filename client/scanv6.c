/*
 * scanv6.c
 * - scan the IPv6 reverse DNS using NXDOMAIN to prune subtrees
 */
/*
 *  This file is
 *   Copyright (C) 2022 Tony Finch <dot@dotat.at>
 *
 *  It is part of adns, which is
 *    Copyright (C) 1997-2000,2003,2006,2014-2016,2020  Ian Jackson
 *    Copyright (C) 2014  Mark Wooding
 *    Copyright (C) 1999-2000,2003,2006  Tony Finch
 *    Copyright (C) 1991 Massachusetts Institute of Technology
 *  (See the file INSTALL for full details.)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation.
 *
 *  This version was originally supplied by Tony Finch, but has been
 *  modified by Ian Jackson as it was incorporated into adns and
 *  subsequently.
 */

#include <sys/types.h>
#include <sys/time.h>

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "config.h"
#include "adns.h"
#include "client.h"

#ifdef ADNS_REGRESS_TEST
# include "hredirect.h"
#endif

/* maximum number of concurrent DNS queries */
#define MAXMAXPENDING 64000
#define DEFMAXPENDING 100
#define MINPENDING 2

static void
usage(void) {
	fprintf(stderr,
		"usage: scanv6 [<options>] <apex>\n"
		"	-c MAX	number of outstanding queries\n"
		"	-d	turn on debugging\n"
		"	-i	debugging invariant checks\n"
		"	-p SECS	report status periodically\n"
		"	-r CONF	instead of resolv.conf\n"
		"	-v	print version\n"
		);
	exit(1);
}

static void
check(const char *name, int e) {
	if(e != 0) {
		err(1, "%s", name);
	}
}

static void
submit(adns_state adns, adns_initflags flags, const char *qname) {
	adns_query q;
	if(flags & adns_if_debug)
		printf("%s submit\n", qname);
	check("adns_submit",
	      adns_submit(adns, qname, adns_r_ptr_raw,
			  adns_qf_owner |
			  adns_qf_quoteok_anshost |
			  adns_qf_quoteok_cname |
			  adns_qf_cname_loose,
			  NULL, &q));
}

int
main(int argc, char *argv[]) {
	size_t maxpending = DEFMAXPENDING;
	const char *resolv_conf = NULL;
	adns_initflags flags = 0;
	int pulse = 0;
	int c;

	while((c = getopt(argc, argv, "c:dip:r:v")) != -1) {
		switch(c) {
		case('c'): {
			int i = atoi(optarg);
			if(i < MINPENDING || i > MAXMAXPENDING) {
				errx(1, "unfeasible concurrency %d", i);
			}
			maxpending = (size_t)i;
			continue;
		}
		case('d'):
			flags |= adns_if_debug;
			continue;
		case('i'):
			flags |= adns_if_checkc_entex;
			continue;
		case('p'):
			pulse = atoi(optarg);
			continue;
		case('r'):
			resolv_conf = optarg;
			continue;

		case('v'):
			printf("%s", VERSION_MESSAGE("scanv6"));
			exit(0);
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if(argc != 1) {
		usage();
	}

	adns_state adns = NULL;
	if(resolv_conf != NULL) {
		check("adns_init_strcfg",
		      adns_init_strcfg(&adns, flags, stderr, resolv_conf));
	} else {
		check("adns_init", adns_init(&adns, flags, stderr));
	}

	time_t beat = time(NULL) + pulse;

	adns_answer **ans = NULL;
	size_t maxans = 0;
	size_t todo = 0;

	submit(adns, flags, *argv);
	size_t pending = 1;

	while(pending > 0 || todo > 0) {
		if(todo == maxans) {
			maxans += maxans + MINPENDING;
			ans = realloc(ans, maxans * sizeof(*ans));
			if(ans == NULL) {
				err(1, "malloc");
			}
		}
		if(pending > 0) {
			adns_query q = NULL;
			adns_answer *a;
			check("adns_wait",
			      adns_wait(adns, &q, &a, NULL));
			pending--;
			switch(a->status) {
			case(adns_s_ok):
				printf("%s PTR %s\n", a->owner, a->rrs.str[0]);
				free(a);
				break;
			case(adns_s_nxdomain):
				if(flags & adns_if_debug)
					printf("%s nxdomain\n", a->owner);
				free(a);
				break;
			case(adns_s_nodata):
				if(strlen(a->owner) < 72) {
					ans[todo++] = a;
					break;
				}
				/* fallthru */
			default:
				printf("%s %s\n", a->owner,
				       adns_strerror(a->status));
				free(a);
				break;
			}
		}
		if(pending < maxpending && todo > 0) {
			char qname[256];
			adns_answer *a = ans[--todo];
			for(c = '0'; c <= '9'; c++) {
				snprintf(qname, sizeof(qname),
					 "%c.%s", c, a->owner);
				submit(adns, flags, qname);
				pending++;
			}
			for(c = 'a'; c <= 'f'; c++) {
				snprintf(qname, sizeof(qname),
					 "%c.%s", c, a->owner);
				submit(adns, flags, qname);
				pending++;
			}
			free(a);
		}
		if(pulse > 0 && time(NULL) > beat) {
			printf("pending %zu todo %zu\n", pending, todo);
			beat = time(NULL) + pulse;
		}
	}

	if(fclose(stdout)) {
		err(1, "stdout");
	}

	return(0);
}
